<?php

/**
 * @file
 * Definition of Drupal\daa\Plugin\views\display\DaaDisplayPluginBase.
 */

namespace Drupal\daa\Plugin\views\display;

use Drupal\views\Plugin\views\display\Page;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a Display test plugin.
 *
 * @Plugin(
 *   id = "domain_views",
 *   title = @Translation("Domain page view"),
 *   uses_hook_menu = TRUE,
 *   contextual_links_locations = {"view"},
 *   theme = "views_view",
 *   admin = @Translation("Domain Page")
 * )
 */
class DaaDisplayPluginBase extends Page {

  /**
   * Whether the display allows attachments.
   *
   * @var bool
   */
  protected $usesAttachments = TRUE;

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::defineOptions().
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['domain_option'] = array('default' => '');

    return $options;
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::optionsSummary().
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $categories['domain_category'] = array(
      'title' => t('Domain settings'),
      'column' => 'second',
      'build' => array(
        '#weight' => -100,
      ),
    );

    $domain_option = $this->getOption('domain_option') ? : t('Empty');

    $options['domain_option'] = array(
      'category' => 'domain_category',
      'title' => t('Domain option'),
      'value' => views_ui_truncate($domain_option, 24),
    );
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::buildOptionsForm().
   */
  public function buildOptionsForm(&$form, &$form_state) {
    parent::buildOptionsForm($form, $form_state);

    switch ($form_state['section']) {
      case 'domain_option':
        $form['#title'] .= t('Domain option');
        $form['domain_option'] = array(
          '#type' => 'textfield',
          '#description' => t('This is a textfield for domain_option.'),
          '#default_value' => $this->getOption('domain_option'),
        );
        break;
    }
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::validateOptionsForm().
   */
  public function validateOptionsForm(&$form, &$form_state) {
    parent::validateOptionsForm($form, $form_state);
    watchdog('views', $form_state['values']['domain_option']);
    switch ($form_state['section']) {
      case 'domain_option':
        if (!trim($form_state['values']['domain_option'])) {
          form_error($form['domain_option'], t('You cannot have an empty option.'));
        }
        break;
    }
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::submitOptionsForm().
   */
  public function submitOptionsForm(&$form, &$form_state) {
    parent::submitOptionsForm($form, $form_state);
    switch ($form_state['section']) {
      case 'domain_option':
        $this->setOption('domain_option', $form_state['values']['domain_option']);
        break;
    }
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::execute().
   */
  public function execute() {
    $this->view->build();

    $render = $this->view->render();
    // Render the test option as the title before the view output.
    $render['#prefix'] = '<h1>' . filter_xss_admin($this->options['domain_option']) . '</h1>';

    return $render;
  }

  /**
   * Overrides Drupal\views\Plugin\views\display\DisplayPluginBase::preview().
   *
   * Override so preview and execute are the same output.
   */
  public function preview() {
    $element = $this->execute();
    return drupal_render($element);
  }
}

