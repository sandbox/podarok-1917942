<?php

/**
 * @file
 * Contains \Drupal\daa\Plugin\daa\Condition\DomainCondition.
 */

namespace Drupal\daa\Plugin\Core\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a 'Domain' condition.
 *
 * @Plugin(
 *   id = "daa_condition",
 *   label = @Translation("Domain Access alt"),
 *   module = "daa",
 *   class = "\Drupal\daa\Plugin\Core\Condition\DomainCondition"
 * )
 */
class DomainCondition extends ConditionPluginBase {

  /**
   * @todo fix this with autocmplete domains selection
   * Overrides \Drupal\Core\Condition\ConditionPluginBase::buildForm().
   */
  public function buildForm(array $form, array &$form_state) {
    $form['domains'] = array(
      '#type' => 'radios',
      '#title' => t('Select domains'),
      '#options' => array('fix_it' => 'fix_it'),
      '#default_value' => '',
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * @broken @todo fix this
   * Overrides \Drupal\Core\Condition\ConditionPluginBase::submitForm().
   */
  public function submitForm(array &$form, array &$form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * @broken @todo fix this
   * Implements \Drupal\Core\Executable\ExecutableInterface::summary().
   */
  public function summary() {
    $language_list = language_list(LANGUAGE_ALL);
    $selected = $this->configuration['langcodes'];
    // Reduce the language list to an array of language names.
    $language_names = array_reduce($language_list, function (&$result, $item) use ($selected) {
      if (!empty($selected[$item->langcode])) {
        $result[$item->langcode] = $item->name;
      }
      return $result;
    }, array());

    // Collate language type names.
    $language_types = language_types_info();
    $language_type_options = array();
    foreach ($language_types as $type_key => $info) {
      if (!empty($info['name'])) {
        $language_type_options[$type_key] = $info['name'];
      }
    }
    if (count($this->configuration['langcodes']) > 1) {
      $last = array_pop($language_names);
      $languages = implode(', ', $language_names);
      return t('The @type language is @not@languages or @last.',
        array(
          '@type' => $language_type_options[$this->configuration['language_type']],
          '@languages' => $languages,
          '@last' => $last,
          '@not' => !empty($this->configuration['negate']) ? 'not ' : ''
        )
      );
    }
    $language = array_pop($language_names);
    return t('The @type language is @not@language.',
      array(
        '@type' => $language_type_options[$this->configuration['language_type']],
        '@language' => $language,
        '@not' => !empty($this->configuration['negate']) ? 'not ' : ''
      )
    );
  }

  /**
   * @broken @todo fix this
   * Implements \Drupal\condition\ConditionInterface::evaluate().
   */
  public function evaluate() {
    return TRUE;
  }

}
