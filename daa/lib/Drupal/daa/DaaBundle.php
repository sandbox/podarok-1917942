<?php

/**
 * @file
 * Definition of Drupal\daa\DaaBundle.
 */

namespace Drupal\daa;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Reference;

/**
 * The bundle for daa.module.
 */
class DaaBundle extends Bundle {
  public function build(ContainerBuilder $container) {
    $container->register('daa.bootstrap', 'Drupal\daa\DomainBootstrap')
      ->addArgument(new Reference('module_handler'))
      ->addArgument(new Reference('request'));
    $container->register('daa.daa_subscriber', 'Drupal\daa\DaaSubscriber')
      ->addTag('event_subscriber')
      ->addArgument(new Reference('daa.bootstrap'))
      ->addArgument(new Reference('config.context'))
      ->addArgument(new Reference('request'));
      //->addArgument(new Reference('plugin.manager.condition'));
  }


}