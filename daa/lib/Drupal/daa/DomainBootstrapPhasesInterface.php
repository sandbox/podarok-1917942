<?php
/**
 * @file
 * Contains Drupal\daa\DomainBootstrapPhasesInterface;
 */

namespace Drupal\daa;


interface DomainBootstrapPhasesInterface {
  /**
   * Domain bootstrap phase 1: makes sure that database is initialized and
   * loads all necessary module files.
   */
  const DOMAIN_BOOTSTRAP_INIT = 0;

  /**
   * Domain bootstrap phase 2: resolves host and does a lookup in the {domain}
   * table. Also invokes hook "hook_domain_bootstrap_lookup".
   */
  const DOMAIN_BOOTSTRAP_NAME_RESOLVE = 1;

  /**
   * Domain bootstrap phase 3: invokes hook_domain_bootstrap_full().
   */
  const DOMAIN_BOOTSTRAP_FULL = 2;
}