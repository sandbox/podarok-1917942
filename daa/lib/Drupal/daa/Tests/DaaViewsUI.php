<?php

/**
 * @file
 * Definition of Drupal\daa\Tests\DaaViewsUI
 */

namespace Drupal\daa\Tests;

use Drupal\domain\Tests;
use Drupal\domain\Plugin\Core\Entity\Domain;

/**
 * Tests the domain record interface.
 */
class DaaViewsUI extends \Drupal\domain\Tests\DomainTestBase {

  public static function getInfo() {
    return array(
      'name' => 'Daa Views UI',
      'description' => 'Tests the daa Views user interface.',
      'group' => 'Daa',
    );
  }

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('domain', 'views', 'daa');

  /**
   * Create, edit and delete a domain via the user interface.
   */
  function testDaaViewsUI() {
    $this->admin_user = $this->drupalCreateUser(array('administer domains'));
    $this->drupalLogin($this->admin_user);

    // Create test domains.
    $this->domainCreateTestDomains(4);

    // Visit the domains views administration page.
    $this->drupalGet('admin/structure/domains');

    // Testing daa_delete_action.
    $edit = array(
      'action_bulk_form[1]' => TRUE,
      'action' => 'daa_delete_action',
    );

    $this->drupalPost('admin/structure/domains', $edit, t('Apply'));
    $this->assertText('Delete domain was applied to 1 item.');

    // Testing daa_set_default_action.
    $edit = array(
      'action_bulk_form[1]' => TRUE,
      'action' => 'daa_set_default_action',
    );
    $this->drupalPost('admin/structure/domains', $edit, t('Apply'));
    $this->assertText('Set default domain was applied to 1 item.');

    // Testing daa_disable_action.
    $edit = array(
      'action_bulk_form[1]' => TRUE,
      'action_bulk_form[2]' => TRUE,
      'action' => 'daa_disable_action',
    );
    $this->drupalPost('admin/structure/domains', $edit, t('Apply'));
    $this->assertText('The default domain cannot be disabled.');
    $this->assertText('Disable domain was applied to 2 items.');

    // Testing daa_enable_action.
    $edit = array(
      'action_bulk_form[2]' => TRUE,
      'action' => 'daa_enable_action',
    );
    $this->drupalPost('admin/structure/domains', $edit, t('Apply'));
    $this->assertText('Enable domain was applied to 1 item.');
  }

}
