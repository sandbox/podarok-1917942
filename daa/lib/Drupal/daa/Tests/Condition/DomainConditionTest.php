<?php

/**
 * @file
 * Contains \Drupal\language\Tests\Condition\LanguageConditionTest.
 */

namespace Drupal\daa\Tests\Condition;

use Drupal\simpletest\DrupalUnitTestBase;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Language\Language;


/**
 * Tests the language condition.
 */
class DomainConditionTest extends DrupalUnitTestBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * @broken @todo fix
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  public static $modules = array('system', 'language');

  /**
   * @broken @todo fix
   * Defines test information.
   */
  public static function getInfo() {
    return array(
      'name' => 'Domain Condition Plugin',
      'description' => 'Tests that the language condition, provided by the language module, is working properly.',
      'group' => 'Condition API',
    );
  }

  /**
   * @broken @todo fix
   * Sets up the tests.
   */
  protected function setUp() {
    parent::setUp();

    $this->installSchema('language', 'language');
    // This is needed for language_default().
    // @todo remove this when language_default() no longer needs variable_get().
    $this->installSchema('system', 'variable');

    // Setup English.
    $default_language = language_save(language_default());

    // Setup Italian.
    $language = new Language(array(
      'langcode' => 'it',
      'name' => 'Italian',
      'direction' => '0',
    ));
    language_save($language);

    $this->manager = new ConditionManager($this->container->getParameter('container.namespaces'));
    $this->languageManager = $this->container->get('language_manager');
  }

  /**
   * @broken @todo fix
   * Tests conditions.
   */
  public function testConditions() {
    // Grab the language condition and configure it to check the content
    // language.
    $condition = $this->manager->createInstance('language')
      ->setConfig('langcodes', array('en' => 'en', 'it' => 'it'))
      ->setConfig('language_type', LANGUAGE_TYPE_CONTENT);
    $this->assertTrue($condition->execute(), 'Language condition passes as expected.');
    // Check for the proper summary.
    $this->assertEqual('The Content language is English or Italian.', $condition->summary());

    // Change to Italian only.
    $condition->setConfig('langcodes', array('it' => 'it'));
    $this->assertFalse($condition->execute(), 'Language condition fails as expected.');
    // Check for the proper summary.
    $this->assertEqual('The Content language is Italian.', $condition->summary());

    // Negate the condition
    $condition->setConfig('negate', TRUE);
    $this->assertTRUE($condition->execute(), 'Language condition passes as expected.');
    // Check for the proper summary.
    $this->assertEqual('The Content language is not Italian.', $condition->summary());

    // Change the default language to Italian.
    $this->languageManager->reset();
    variable_set('language_default', array(
      'langcode' => 'it',
      'name' => 'Italian',
      'direction' => 0,
      'weight' => 0,
      'locked' => 0,
    ));
    $this->languageManager->init();

    // Use the language interface now.
    $condition = $this->manager->createInstance('language')
      ->setConfig('langcodes', array('en' => 'en', 'it' => 'it'))
      ->setConfig('language_type', LANGUAGE_TYPE_INTERFACE);

    $this->assertTrue($condition->execute(), 'Language condition passes as expected.');
    // Check for the proper summary.
    $this->assertEqual('The User interface text language is English or Italian.', $condition->summary());

    // Change to Italian only.
    $condition->setConfig('langcodes', array('it' => 'it'));
    $this->assertTrue($condition->execute(), 'Language condition passes as expected.');
    // Check for the proper summary.
    $this->assertEqual('The User interface text language is Italian.', $condition->summary());

    // Negate the condition
    $condition->setConfig('negate', TRUE);
    $this->assertFalse($condition->execute(), 'Language condition fails as expected.');
    // Check for the proper summary.
    $this->assertEqual('The User interface text language is not Italian.', $condition->summary());
  }

}
