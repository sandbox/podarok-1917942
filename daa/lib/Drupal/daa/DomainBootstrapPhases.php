<?php
/**
 * @file
 * Contains Drupal\daa\DomainBootstrap\DomainBootstrapPhases class
 */

namespace Drupal\daa\DomainBootstrap;


class DomainBootstrapPhases {
  /**
   * Domain bootstrap phase 1: makes sure that database is initialized and
   * loads all necessary module files.
   */
  const DOMAIN_BOOTSTRAP_INIT = 0;

  /**
   * Domain bootstrap phase 2: resolves host and does a lookup in the {domain}
   * table. Also invokes hook "hook_domain_bootstrap_lookup".
   */
  const DOMAIN_BOOTSTRAP_NAME_RESOLVE = 1;

  /**
   * Domain bootstrap phase 3: invokes hook_domain_bootstrap_full().
   */
  const DOMAIN_BOOTSTRAP_FULL = 2;

  /**
   * array stores all bootstrap phases
   */
  static $phases;

  /**
   * initialization
   */
  function __construct() {
    self::$phases = new ArrayObject(array(
      DOMAIN_BOOTSTRAP_INIT,
      DOMAIN_BOOTSTRAP_NAME_RESOLVE,
      DOMAIN_BOOTSTRAP_FULL,
    ), ArrayObject::STD_PROP_LIST);
  }

  /**
   * Returns phases array
   */
  protected function getPhases() {
    return self::$phases;
  }

  /**
   * Adds user level bootstrap phase
   */
  static function addPhases($_phases = array()) {
    array_push(self::$phases, $_phases);
  }


}