<?php
/**
 * @file
 * Contains Drupal\daa\DomainBootstrapInterface
 *
 * @todo create docs
 */

namespace Drupal\daa;
use ArrayObject;


interface DomainBootstrapInterface {

  /**
   * @todo update docs for former domain_bootstrap()
   * @todo create change record for this
   * Domain module bootstrap: calls all bootstrap phases.
   *
   * Effectively, we add our own bootstrap phase to Drupal core.
   * This allows us to do selective configuration changes before Drupal
   * has a chance to react. For example, the Domain Conf module allows
   * page caching to be set to "on" for example.com but "off" for
   * no-cache.example.com.
   *
   * The big issues here are command-line interactions, which are slightly
   * different; and the fact that drupal_settings_initialize() is needed to
   * properly hit the page cache.
   *
   * The initial check for GLOBALS['base_root'] lets us know that we have
   * already run the normal configuration routine and are now free to alter
   * settings per-domain.
   *
   * Note that this problem is largely caused by the need to jump ahead
   * in drupal_bootstrap() in order to have database functions available.
   *
   */
  public function domain_bootstrap();

}