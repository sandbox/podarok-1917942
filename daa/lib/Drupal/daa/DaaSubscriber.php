<?php
/**
 * @file
 * Definition of Drupal\daa\DaaSubscriber.
 */

namespace Drupal\daa;

use Doctrine\Tests\Common\Annotations\True;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\daa\DomainBootstrap;
use Drupal\Core\Condition\ConditionManager;
use Drupal\daa\Plugin\Core\Condition\DomainCondition;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\Context\ConfigContext;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\Core\Config\ConfigEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Drupal;


/**
 * Implements DaaSubscriber
 */
class DaaSubscriber implements EventSubscriberInterface {

  /**
   * The DomainBootstrap to get the Daa working.
   *
   * @var \Drupal\daa\DomainBootstrap
   */
  protected $domainbootstrap;
  /**
   * The ConditionManager to get the DomainCondition working.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $condition;
  /**
   * The Request object to get the Request data.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Config\Context\ConfigContext
   */
  protected $defaultConfigContext;

  /**
   * Constructs a DaaSubscriber object.
   *
   * @param \Drupal\daa\DomainBootstrap $domainbootstrap
   * DomainBootstrap reference
   * @param \Drupal\Core\Config\Context\ContextInterface $config_context
   * The config context service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   * Request object
   * @param \Drupal\Core\Condition\ConditionManager
   * ConditionManager object for Domain conditions
   */
  public function __construct(DomainBootstrap $domainbootstrap, ContextInterface $config_context, Request $request /*, ConditionManager $condition*/) {
    if (!$this->domainbootstrap) {
      $this->domainbootstrap = $domainbootstrap;
    }
    if (!$this->request) {
      $this->request = $request;
    }
    if (!$this->defaultConfigContext) {
      $this->defaultConfigContext = $config_context;
    }
  }

  /**
   * Gets DomainCondition object
   * @return DomainCondition
   */
  public function getDomainCondition() {

    // @todo fix condition
    if (!$this->condition) {
      $this->condition = new ConditionManager(Drupal::$container->getParameter('container.namespaces'));
//      if (!$this->condition->getDefinition('daa_condition')) {
      return $this->condition->createInstance('daa_condition');
//      }
    }
  }

  /**
   * Initialize configuration context with language.
   *
   * @param \Drupal\Core\Config\ConfigEvent $event
   *   The Event to process.
   */
  public function configContext(ConfigEvent $event) {
    $context = $event->getContext();

    // Add user's language for user context.
    // $this->request->getHttpHost();
    $context->set('domain.hostname', $_SERVER['HTTP_HOST']);
  }

  /**
   * Override configuration values with localized data.
   *
   * @param \Drupal\Core\Config\ConfigEvent $event
   *   The Event to process.
   */
  public function configLoad(ConfigEvent $event) {
    $context = $event
      ->getContext();

    if ($hostname = $context
      ->get('domain.hostname')
    ) {
      // check for already rewrited config names
      // @todo fix this
      if (strpos($event->getConfig()->getName(), 'domain.config') !== TRUE) {
        $config = $event
          ->getConfig();

        // set new config name
        $config = $event
          ->getConfig()
          ->setName($this
            ->getDomainConfigName($config
              ->getName(), $hostname));
      }
    }
  }

  /**
   * Override configuration names with domain prefix.
   *
   * @param \Drupal\Core\Config\ConfigEvent $event
   *   The Event to process.
   */
  public function configSave(ConfigEvent $event) {
    // check for already rewrited config names
    // @todo fix this
    if (strpos($event->getConfig()->getName(), 'domain.config') !== FALSE) {
      // get data for save
      $data = $event->getConfig()
        ->getStorage()
        ->read($event
          ->getConfig()
          ->getName());
      // save data to domain prefixed configname
      $event
        ->getConfig()
        ->getStorage()
        ->write($this
          ->getDomainConfigName($event
            ->getConfig()
            ->getName(), $_SERVER['HTTP_HOST']), $data);
    }
  }

  public function onKernelRequestSetDefaultConfigContextHostname(GetResponseEvent $event) {
    $this->defaultConfigContext->set('domain.hostname', $_SERVER['HTTP_HOST']);
  }

  /**
   * Get configuration name for this hostname.
   *
   * It will be the same name with a prefix depending on language code:
   * locale.config.LANGCODE.NAME
   *
   * @param string $name
   *   The name of the config object.
   * @param \Drupal\Core\Language\Language $language
   *   The language object.
   *
   * @return string
   *   The localised config name.
   */
  public function getDomainConfigName($name, $hostname) {
    //$domain = parse_url($hostname, PHP_URL_HOST);
    return 'domain.config.' . strtr($hostname, array('.' => '-', ':' => '-')) . '.' . $name;
  }


  /**
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   */
  public function DaaLoad(GetResponseEvent $event) {
    // @todo remove this debug code
    drupal_set_message('Daa: subscribed domain_bootstrap');
    $this->domainbootstrap->domain_bootstrap();
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   */
  static function getSubscribedEvents() {
    $events['config.context'][] = array('configContext', 25);
    $events['config.load'][] = array('configLoad', 20);
    $events['config.save'][] = array('configSave', 20);
    $events[KernelEvents::REQUEST][] = array('onKernelRequestSetDefaultConfigContextHostname', 30);
    $events[KernelEvents::REQUEST][] = array('DaaLoad', 9999);
    return $events;
  }
}




