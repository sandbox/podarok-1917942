<?php

/**
 * @file
 * Provide views data and handlers for daa.module.
 *
 * @ingroup views_module_handlers
 */

function daa_views_data() {
  // domain table -- basic table information

  // Define tables for making possible use domain
  // entity in Views.

  $data['domain']['table']['group'] = t('Domains');
  $data['domain']['table']['entity type'] = 'domain';

  // Advertise this table as a possible base table
  $data['domain']['table']['base'] = array(
    'field' => 'domain_id',
    'title' => t('Domain'),
    'weight' => -10,
    'access query tag' => 'domain_access',
  );

  // domain table -- fields

  // domain_id numeric text field.
  $data['domain']['domain_id'] = array(
    'title' => t('Domain ID'),
    'help' => t('The domain ID.'), // The help that appears on the UI,
    // Information for displaying the domain_id
    'field' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  // hostname text field.
  $data['domain']['hostname'] = array(
    'title' => t('Hostname'),
    'help' => t('Just a hostname field.'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  // name text field.
  $data['domain']['name'] = array(
    'title' => t('Domain name'),
    'help' => t('Just a Domain name field.'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  // name text field.
  $data['domain']['scheme'] = array(
    'title' => t('Scheme'),
    'help' => t('Just a scheme field.'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  // status boolean field.
  $data['domain']['status'] = array(
    'title' => t('Status'),
    'help' => t('Just an on/off status field.'),
    'field' => array(
      'id' => 'boolean',
    ),
    'filter' => array(
      'id' => 'boolean',
      // Note that you can override the field-wide label:
      'label' => t('Active'),
      // This setting is used by the boolean filter handler, as possible option.
      'type' => 'yes-no',
      // use boolean_field = 1 instead of boolean_field <> 0 in WHERE statment.
      'use_equal' => TRUE,
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  // weight numeric text field.
  $data['domain']['weight'] = array(
    'title' => t('Domain weight'),
    'help' => t('The domain weight field.'), // The help that appears on the UI,
    // Information for displaying the domain_id
    'field' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  // is_default numeric text field.
  $data['domain']['is_default'] = array(
    'title' => t('Domain is default'),
    'help' => t('The domain is_default field.'), // The help that appears on the UI,
    // Information for displaying the domain_id
    'field' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  // name text field.
  $data['domain']['machine_name'] = array(
    'title' => t('Domain machine name'),
    'help' => t('Just a machine_name field.'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  return $data;
}
