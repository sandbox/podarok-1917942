<?php

/**
 * @file
 * Definition of DomainStorageController.
 */

namespace Drupal\domain;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\DatabaseStorageController;

/**
 * Defines a controller class for domain records.
 */
class DomainStorageController extends DatabaseStorageController {

}
